import svgwrite as svg
from Castle import castle
from Castle.castle import cm
from Castle.castle import ParameterClass
from Castle.castle import Platform


drawing = svg.Drawing('Finals/CompleteCastle/CompleteCastle.svg', profile='full')

space = 0.15*cm()


## Triple blocks with arc entrance, slits up (2x)
arc = ParameterClass()
arc.arcBlocks = [[2]]
arc.arcOffsetX = 0.0*cm()
arc.arcHeightF = 0.7
arc.arcQF = 0.4

for xi in range(1):
    for yi in range(2):
        x = xi*(6.0*cm()+space)
        y = yi*(6.0*cm()+space)
        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                      blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                      connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                      slitLUBlocks=[1,3], slitRUBlocks=[1,3],
                      arcBlocks=arc.arcBlocks, arcOffsetX=arc.arcOffsetX, arcHeightF=arc.arcHeightF, arcQF=arc.arcQF)

## Triple blocks, slits up (2x)
for xi in range(1):
    for yi in range(2,4):
        x = xi*(6.0*cm()+space)
        y = yi*(6.0*cm()+space)
        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                      blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                      connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                      slitLUBlocks=[1,3], slitRUBlocks=[1,3])



## Triple blocks, slits down (4x)
for xi in range(3,4):
    for yi in range(4):
        x = xi*(6.0*cm()+space)
        y = yi*(6.0*cm()+space)
        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                      blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                      connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                      slitLDBlocks=[1,3], slitRDBlocks=[1,3])



## Single blocks, slits up (2x)
for xi in range(6,8):
    for yi in range(4):
        x = xi*(6.0*cm()+space)
        y = yi*(6.0*cm()+space)

        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                      blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                      connBlocksTop=[1], connBlocksBot=[1], platformSlitBlocks=[1], windowBlocks = [1],
                      slitLUBlocks = [1], slitRUBlocks = [1])

## Single blocks, slits down (2x)
for xi in range(8,10):
    for yi in range(4):
        x = xi*(6.0*cm()+space)
        y = yi*(6.0*cm()+space)

        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                      blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                      connBlocksTop=[1], connBlocksBot=[1], platformSlitBlocks=[1], windowBlocks = [1],
                      slitLDBlocks = [1], slitRDBlocks = [1])



## Kanteel blocks, slits up (2x)
for xi in range(10,11):
    for yi in range(6):
        x = xi*(6.0*cm()+space)
        y = yi*(4.0*cm()+space)

        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                  blockWidth=6.0*cm(), blockHeight=4.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[], connBlocksBot=[1], noConnBlocksTop = [1], platformSlitBlocks=[], windowBlocks = [],
                  slitLUBlocks = [1], slitRUBlocks = [1], crenBlocks = [[1]], crenOffsetY=0.0)

for xi in range(11,12):
    for yi in range(6):
        x = xi*(6.0*cm()+space)
        y = yi*(4.0*cm()+space)

        castle.Castle(save=False, drawing=drawing, startX=x, startY = y,
                  blockWidth=6.0*cm(), blockHeight=4.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[], connBlocksBot=[1], noConnBlocksTop = [1], platformSlitBlocks=[], windowBlocks = [],
                  slitLDBlocks = [1], slitRDBlocks = [1], crenBlocks = [[1]], crenOffsetY=0.0)

"""
## Platforms
for xi in range(10,12):
    for yi in range(4):
        x = xi*(6.0*cm()+space)
        y = yi*(6.0*cm()+space)

        Platform(save=False, drawing=drawing, startX=x, startY = y)

"""




drawing.save()