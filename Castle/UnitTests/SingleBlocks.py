from Castle import castle
from Castle.castle import cm
from Castle.castle import ParameterClass

"""
Single full blocks
"""

arc = ParameterClass()
arc.arcOffsetX = 1.6*cm()
arc.arcHeightF = 0.6
arc.arcQF = 0.2
arc.arcBlocks = [[1]]


a = castle.Castle(filename='./Finals/Single/SingleBlock_topslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[1], platformSlitBlocks=[1], windowBlocks = [1],
                  slitLUBlocks = [1], slitRUBlocks = [1])

a = castle.Castle(filename='./Finals/Single/SingleBlock_botslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[1], platformSlitBlocks=[1], windowBlocks = [1],
                  slitLDBlocks = [1], slitRDBlocks = [1])

a = castle.Castle(filename='./Finals/Single/SingleBlock_hybridslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[1], platformSlitBlocks=[1], windowBlocks = [1],
                  slitLDBlocks = [1], slitRUBlocks = [1])

a = castle.Castle(filename='./Finals/Single/SingleBlock_botslit_floor.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[], platformSlitBlocks=[1], windowBlocks = [1],
                  slitLDBlocks = [1], slitRDBlocks = [1])

a = castle.Castle(filename='./Finals/Single/SingleBlock_topslit_floor.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[], platformSlitBlocks=[1], windowBlocks = [1],
                  slitLUBlocks = [1], slitRUBlocks = [1])

a = castle.Castle(filename='./Finals/Single/SingleBlock_botslit_door.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[], platformSlitBlocks=[1], windowBlocks = [],
                  slitLDBlocks=[1], slitRDBlocks = [1],
                  arcBlocks=arc.arcBlocks, arcOffsetX=arc.arcOffsetX, arcHeightF=arc.arcHeightF, arcQF=arc.arcQF)

a = castle.Castle(filename='./Finals/Single/SingleBlock_topslit_door.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[1], connBlocksBot=[], platformSlitBlocks=[1], windowBlocks = [],
                  slitLUBlocks=[1], slitRUBlocks = [1],
                  arcBlocks=arc.arcBlocks, arcOffsetX=arc.arcOffsetX, arcHeightF=arc.arcHeightF, arcQF=arc.arcQF)
