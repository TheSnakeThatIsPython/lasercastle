from Castle import castle
from Castle.castle import cm
from Castle.castle import ParameterClass

"""
Connection blocks
"""

arc = ParameterClass()
arc.arcBlocks = [[2]]
arc.arcOffsetX = 0.0*cm()
arc.arcHeightF = 0.7
arc.arcQF = 0.4

a = castle.Castle(filename='Finals/Connections/Conn_topslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=7, blockVertN=1,
                  connblocks=[1,3,5,7], platformSlitBlocks=[1,3,5], windowBlocks = [1,3,5,7], crenBlocks = [[2],[4],[6]],
                  slitLUBlocks = [1,3,5,7], slitRUBlocks = [1,3,5,7], cutLineBlocks=[3,5])


a = castle.Castle(filename='Finals/Connections/Conn_botslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=7, blockVertN=1,
                  connblocks=[1,3,5,7], platformSlitBlocks=[1,3,5], windowBlocks = [1,3,5,7], crenBlocks = [[2],[4],[6]],
                  slitLDBlocks = [1,3,5,7], slitRDBlocks = [1,3,5,7], cutLineBlocks=[3,5])

