from Castle import castle
from Castle.castle import cm
from Castle.castle import ParameterClass




a = castle.Castle(filename='./Finals/KanteelBlocks/Kanteel_topslit.svg', blockWidth=6.0*cm(), blockHeight=4.0*cm(), blockHorzN=1, blockVertN=1,
                  connBlocksTop=[], connBlocksBot=[1], noConnBlocksTop = [1], platformSlitBlocks=[], windowBlocks = [],
                  slitLUBlocks = [1], slitRUBlocks = [1], crenBlocks = [[1]], crenOffsetY=0.0)