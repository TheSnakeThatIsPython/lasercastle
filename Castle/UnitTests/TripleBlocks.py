from Castle import castle
from Castle.castle import cm
from Castle.castle import ParameterClass

"""
Triple Blocks
"""


arc = ParameterClass()
arc.arcBlocks = [[2]]
arc.arcOffsetX = 0.0*cm()
arc.arcHeightF = 0.7
arc.arcQF = 0.4


a = castle.Castle(filename='Finals/Triple/Triple_topslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                  connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                  slitLUBlocks = [1,3], slitRUBlocks = [1,3])

a = castle.Castle(filename='Finals/Triple/Triple_botslit.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                  connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                  slitLDBlocks = [1,3], slitRDBlocks = [1,3])


a = castle.Castle(filename='Finals/Triple/Triple_topslit_door.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                  connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                  slitLUBlocks = [1,3], slitRUBlocks = [1,3],
                  arcBlocks=arc.arcBlocks, arcOffsetX=arc.arcOffsetX, arcHeightF=arc.arcHeightF, arcQF=arc.arcQF)

a = castle.Castle(filename='Finals/Triple/Triple_botslit_door.svg', blockWidth=6.0*cm(), blockHeight=6.0*cm(), blockHorzN=3, blockVertN=1,
                  connblocks=[1,3], platformSlitBlocks=[1,3], windowBlocks = [1,3], crenBlocks = [[2]],
                  slitLDBlocks = [1,3], slitRDBlocks = [1,3],
                  arcBlocks=arc.arcBlocks, arcOffsetX=arc.arcOffsetX, arcHeightF=arc.arcHeightF, arcQF=arc.arcQF)

