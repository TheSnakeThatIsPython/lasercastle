import svgwrite as svg
import math


class ParameterClass():
    pass


def cm():
    return 35.43307*1.0664


class SlitConnector:
    startX = 0.0*cm()
    startY = 0.0*cm()
    blockWidth = 6.0*cm()
    midSlitDepth = 1.5*cm()

    slitOffset = 0.8*cm()
    slitWidth = 0.29*cm()

    def __init__(self, **kwargs):
        for key,value in kwargs.iteritems():

            if key.lower() in ("filename"):
                self.filename = value


        self.drawing = svg.Drawing(self.filename, profile='full')

        self.Outline()

        self.drawing.save()

    def Outline(self, strokeColor='black', strokeWidth=1, clickout = 0.015*cm()):
        path = []
        path.append(('M', self.startX+clickout, self.startY))
        path.append(('l', -clickout+self.blockWidth-clickout, self.startY))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


        path = []
        path.append(('M', self.startX+self.blockWidth, self.startY+clickout))
        path.append(('l', 0.0, -clickout+self.midSlitDepth*2.0))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


        path = []
        path.append(('M', self.startX+self.blockWidth-clickout, self.midSlitDepth*2.0))
        path.append(('l', -self.slitOffset, 0.0))

        path.append(('l', 0.0, -self.midSlitDepth))
        path.append(('l', -self.slitWidth, 0.0))
        path.append(('l', 0.0, +self.midSlitDepth))

        path.append(('l', -self.blockWidth+2.0*(self.slitOffset+self.slitWidth), 0.0))
        path.append(('l', 0.0, -self.midSlitDepth))
        path.append(('l', -self.slitWidth, 0.0))
        path.append(('l', 0.0, +self.midSlitDepth))
        path.append(('l', -self.slitOffset+clickout, 0.0))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


        path = []
        path.append(('M', self.startX, self.startY+self.midSlitDepth*2.0-clickout))
        path.append(('L', self.startX, self.startY+clickout))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)





class Platform:

    save = False
    drawing = None

    startX = 0.0*cm()
    startY = 0.0*cm()
    blockWidth = 6.0*cm()

    platformSlitWidth = 0.6*cm()
    platformSlitHeight = 0.3*cm()

    slitWidth = 0.29*cm()
    slitOffset = 0.8*cm()

    clearance = 0.05*cm()

    def __init__(self, **kwargs):
        for key,value in kwargs.iteritems():

            if key.lower() in ("filename"):
                self.filename = value

            elif key.lower() in ("save"):
                self.save = value

            elif key.lower() in ("drawing"):
                self.drawing = value

            elif key.lower() in ("startx"):
                self.startX = value

            elif key.lower() in ("starty"):
                self.startY = value


        if self.drawing == None:
            self.drawing = svg.Drawing(self.filename, profile='full')

        self.SinglePlatform()
        self.Outline()

        if self.save:
            self.drawing.save()


    def SinglePlatform(self):
        self.platformWidth = self.blockWidth-2.0*(self.slitOffset+self.slitWidth+self.clearance)
        self.platformHeight = self.platformWidth

        self.clickWidth = self.platformSlitWidth
        self.clickDepth = self.platformSlitHeight+self.clearance


    def Outline(self, strokeColor='black', strokeWidth=1, clickout = 0.015*cm()):
        path = []
        path.append(('M', self.startX+self.clickDepth+clickout, self.startY+self.clickDepth))
        path.append(('l', -clickout+self.platformWidth/2.0-self.clickWidth/2.0, 0.0))
        path.append(('l', 0.0, -self.clickDepth))
        path.append(('l', self.clickWidth, 0.0))
        path.append(('l', 0.0, self.clickDepth))
        path.append(('l', self.platformWidth/2.0-self.clickWidth/2.0-clickout, 0.0))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


        path = []
        path.append(('M', self.startX+self.clickDepth+self.platformWidth, self.startY+self.clickDepth+clickout))
        path.append(('l', 0.0, -clickout+self.platformWidth/2.0-self.clickWidth/2.0))
        path.append(('l', self.clickDepth, 0.0))
        path.append(('l', 0.0, self.clickWidth))
        path.append(('l', -self.clickDepth, 0.0))
        path.append(('l', 0.0, self.platformHeight/2.0-self.clickWidth/2.0-clickout))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


        path = []
        path.append(('M', self.startX+self.clickDepth+self.platformWidth-clickout, self.startY+self.clickDepth+self.platformHeight))
        path.append(('l', +clickout-self.platformWidth/2.0+self.clickWidth/2.0, 0.0))
        path.append(('l', 0.0, +self.clickDepth))
        path.append(('l', -self.clickWidth, 0.0))
        path.append(('l', 0.0, -self.clickDepth))
        path.append(('l', -self.platformWidth/2.0+self.clickWidth/2.0+clickout, 0.0))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


        path = []
        path.append(('M', self.startX+self.clickDepth+clickout, self.startY+self.clickDepth+self.platformHeight))
        path.append(('l', 0.0, +clickout-self.platformWidth/2.0+self.clickWidth/2.0))
        path.append(('l', -self.clickDepth, 0.0))
        path.append(('l', 0.0, -self.clickWidth))
        path.append(('l', +self.clickDepth, 0.0))
        path.append(('l', 0.0, -self.platformHeight/2.0+self.clickWidth/2.0+clickout))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)



class Castle:
    """A class of a castle"""
    save = True
    drawing = None

    startX = 0.0*cm()
    startY = 0.0*cm()

    blockWidth = 6.0*cm()
    blockHeight = 6.0*cm()
    blockHorzN = 1
    blockVertN = 1

    connWidth1 = 0.8*cm()
    connWidth2 = connWidth1+0.6*cm()
    connHeight = 0.8*cm()
    connBlocksTop = []
    connBlocksBot = []
    noConnBlocksTop = []

    platformSlitBlocks = []
    platformSlitWidth = 0.6*cm()
    platformSlitHeight = 0.3*cm()

    cutLineBlocks = []


    windowBlocks = []
    windowHeight = 2.5*cm()
    windowWidth = 1.0*cm()
    windowFac = 0.7
    windowOffsetX = 0.0*cm()
    windowOffsetY = 0.0*cm()

    midSlitBlocks = []
    midSlitDepth = 1.5*cm()

    slitLUBlocks = []
    slitRUBlocks = []
    slitLDBlocks = []
    slitRDBlocks = []
    slitWidth = 0.29*cm()
    slitHeightF = 0.5
    slitHeight = 0.0*cm() # this will be recalculated
    slitOffset = 0.8*cm()
    slitCorrHeight = 0.6*cm()
    slitCorrWidth = 0.6*cm()


    arcBlocks = []
    arcOffsetX = 0.0*cm()
    arcHeightF = 0.7
    arcQF = 0.4

    crenBlocks = []
    crenHeight = 0.8*cm()
    crenWidth = 0.8*cm()
    crenOffsetY = connHeight



    def __init__(self, **kwargs):
        # Parse the input values
        for key,value in kwargs.iteritems():
            if key.lower() in ("blockwidth"):
                self.blockWidth = value

            elif key.lower() in ("blockheight"):
                self.blockHeight = value

            elif key.lower() in ("blockhorzn"):
                self.blockHorzN = value

            elif key.lower() in ("blockvertn"):
                self.blockVertN = value

            elif key.lower() in ("startx"):
                self.startX = value

            elif key.lower() in ("starty"):
                self.startY = value

            elif key.lower() in ("filename"):
                self.filename = value

            elif key.lower() in ("connblockstop"):
                self.connBlocksTop = value

            elif key.lower() in ("connblocksbot"):
                self.connBlocksBot = value

            elif key.lower() in ("platformslitblocks"):
                self.platformSlitBlocks = value

            elif key.lower() in ("platformslitwidth"):
                self.platformSlitWidth = value

            elif key.lower() in ("platformslitheight"):
                self.platformSlitHeight = value

            elif key.lower() in ("cutlineblocks"):
                self.cutLineBlocks = value

            elif key.lower() in ("windowblocks"):
                self.windowBlocks = value

            elif key.lower() in ("windowheight"):
                self.windowHeight = value

            elif key.lower() in ("windowwidth"):
                self.windowWidth = value

            elif key.lower() in ("windowfac"):
                self.windowFac = value

            elif key.lower() in ("windowoffsetx"):
                self.windowOffsetX = value

            elif key.lower() in ("windowoffsety"):
                self.windowOffsetY = value

            elif key.lower() in ("slitlublocks"):
                self.slitLUBlocks = value

            elif key.lower() in ("slitrublocks"):
                self.slitRUBlocks = value

            elif key.lower() in ("slitldblocks"):
                self.slitLDBlocks = value

            elif key.lower() in ("slitrdblocks"):
                self.slitRDBlocks = value

            elif key.lower() in ("midslitblocks"):
                self.midSlitBlocks = value

            elif key.lower() in ("arcblocks"):
                self.arcBlocks = value

            elif key.lower() in ("archeightf"):
                self.arcHeightF = value

            elif key.lower() in ("arcoffsetx"):
                self.arcOffsetX = value

            elif key.lower() in ("arcqf"):
                self.arcQF = value

            elif key.lower() in ("crenblocks"):
                self.crenBlocks = value

            elif key.lower() in ("crenoffsety"):
                self.crenOffsetY = value

            elif key.lower() in ("noconnblockstop"):
                self.noConnBlocksTop = value

            elif key.lower() in ("save"):
                self.save = value

            elif key.lower() in ("drawing"):
                self.drawing = value


        """
        # Post parse the inputs
        if len(self.connBlocksTop)==0:
            self.connBlocksTop = [1,self.blockHorzN]
        """


        self.totalWidth = self.blockWidth*self.blockHorzN
        self.totalHeight = self.blockHeight*self.blockVertN

        self.slitHeight = self.totalHeight*self.slitHeightF

        self.arcHeight = self.arcHeightF*(self.totalHeight-self.connHeight)

        # Prepare drawing
        if self.drawing == None:
            self.drawing = svg.Drawing(self.filename, profile='full')




        # Make everything
        self.Outline(strokeColor='green')
        self.BottomConnections(strokeColor='blue')
        self.TopConnections(strokeColor='blue')
        self.PlatFormSlits(strokeColor='blue')
        self.CutLines(strokeColor='blue')
        self.Windows(strokeColor='blue')
        self.Slits(strokeColor='blue')
        self.Arcs(strokeColor='blue')
        self.Crennelations(strokeColor='blue')

        if self.save:
            self.drawing.save()


    def Outline(self, strokeColor='black', strokeWidth=1, clickout = 0.015*cm()):

        # Top part
        path = []
        path.append(('M', self.startX, self.startY+self.totalHeight/2.0-clickout))
        path.append(('L', self.startX, self.startY))
        path.append(('L',self.startX+self.totalWidth,self.startY))
        path.append(('L', self.startX+self.totalWidth, self.startY+self.totalHeight/2.0-clickout))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)

        # Bottom part
        path = []
        path.append(('M', self.startX+self.totalWidth, self.startY+self.totalHeight/2.0+clickout))
        path.append(('L', self.startX+self.totalWidth, self.startY+self.totalHeight))
        path.append(('L', self.startX, self.startY+self.totalHeight))
        path.append(('L', self.startX, self.startY+self.totalHeight/2.0+clickout))

        pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
        self.drawing.add(pathObj)


    def Crennelations(self, strokeColor='green', strokeWidth=1):
        for cren in self.crenBlocks:
            if len(cren)==1:
                startBlock = cren[0]
                stopBlock = cren[0]
            else:
                startBlock = cren[0]
                stopBlock = cren[1]

            nCren = math.ceil(self.blockWidth*(stopBlock-startBlock+1)/self.crenWidth)
            if nCren%2==0:
                nCren = nCren+1

            realW = self.blockWidth*(stopBlock-startBlock+1)/nCren

            startX = self.startX+(startBlock-1)*self.blockWidth
            startY = self.startY+self.crenOffsetY
            downY = 0.0

            curveString = 'M'
            path = []
            for i in range(int(nCren)):
                path.append((curveString, startX, startY+downY))
                curveString = 'L'
                path.append((curveString, startX+realW, startY+downY))

                startX = startX + realW

                if downY == 0.0:
                    downY = self.crenHeight
                else:
                    downY = 0.0

            pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
            self.drawing.add(pathObj)


    def Arcs(self, strokeColor='red', strokeWidth=1):
        for arc in self.arcBlocks:

            if len(arc)==1:
                startBlock = arc[0]
                stopBlock = arc[0]
            else:
                startBlock = arc[0]
                stopBlock = arc[1]

            path = []
            path.append(('M', self.startX+(startBlock-1)*self.blockWidth+self.arcOffsetX,
                         self.startY+self.totalHeight))

            path.append(('Q', self.startX+(startBlock-1)*self.blockWidth+self.blockWidth*(stopBlock-startBlock+1)/2.0-self.blockWidth*(stopBlock-startBlock+1)*self.arcQF,
                         self.startY+self.totalHeight-self.arcHeight,
                         self.startX+(startBlock-1)*self.blockWidth+self.blockWidth*(stopBlock-startBlock+1)/2.0,
                         self.startY+self.totalHeight-self.arcHeight))


            path.append(('Q', self.startX+(startBlock-1)*self.blockWidth+self.blockWidth*(stopBlock-startBlock+1)/2.0+self.blockWidth*(stopBlock-startBlock+1)*self.arcQF,
                         self.startY+self.totalHeight-self.arcHeight,
                         self.startX+(stopBlock)*self.blockWidth-self.arcOffsetX,
                         self.startY+self.totalHeight))

            pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
            self.drawing.add(pathObj)

    def Slits(self, strokeColor='red', strokeWidth=1):

        for i in self.midSlitBlocks:
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0-self.slitWidth/2.0,
                         self.startY))
            path.append(('l', 0.0, self.midSlitDepth))
            path.append(('l', self.slitWidth, 0.0))
            path.append(('l', 0.0, -self.midSlitDepth))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)


        for i in self.slitLUBlocks:
            # Main slit
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.slitOffset,
                         self.startY))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset,
                         self.startY+self.slitHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset+self.slitWidth,
                         self.startY+self.slitHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset+self.slitWidth,
                         self.startY))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)

            if len(self.connBlocksBot)>0:
                # Correction is only necessary when not a floorpiece
                path = []
                path.append(('M', self.startX+(i-1)*self.blockWidth+self.slitOffset,
                             self.startY+self.totalHeight))
                path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset,
                             self.startY+self.totalHeight-self.slitCorrHeight))
                path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset+self.slitCorrWidth,
                             self.startY+self.totalHeight-self.slitCorrHeight))
                path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset+self.slitCorrWidth,
                             self.startY+self.totalHeight))

                pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
                self.drawing.add(pathObj)





        for i in self.slitRUBlocks:
            path = []
            path.append(('M', self.startX+(i)*self.blockWidth-self.slitOffset,
                         self.startY))
            path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset,
                         self.startY+self.slitHeight))
            path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset-self.slitWidth,
                         self.startY+self.slitHeight))
            path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset- self.slitWidth,
                         self.startY))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)

            if len(self.connBlocksBot)>0:
                # Correction is only necessary when not a floorpiece
                path = []
                path.append(('M', self.startX+(i)*self.blockWidth-self.slitOffset,
                             self.startY+self.totalHeight))
                path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset,
                             self.startY+self.totalHeight-self.slitCorrHeight))
                path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset-self.slitCorrWidth,
                             self.startY+self.totalHeight-self.slitCorrHeight))
                path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset-self.slitCorrWidth,
                             self.startY+self.totalHeight))

                pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
                self.drawing.add(pathObj)


        for i in self.slitLDBlocks:
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.slitOffset,
                         self.startY+self.totalHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset,
                         self.startY+self.totalHeight-self.slitHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset+self.slitWidth,
                         self.startY+self.totalHeight-self.slitHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.slitOffset+self.slitWidth,
                         self.startY+self.totalHeight))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)

        for i in self.slitRDBlocks:
            path = []
            path.append(('M', self.startX+(i)*self.blockWidth-self.slitOffset,
                         self.startY+self.totalHeight))
            path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset,
                         self.startY+self.totalHeight-self.slitHeight))
            path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset-self.slitWidth,
                         self.startY+self.totalHeight-self.slitHeight))
            path.append(('L', self.startX+(i)*self.blockWidth-self.slitOffset-self.slitWidth,
                         self.startY+self.totalHeight))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)


    def Windows(self, strokeColor='green', strokeWidth=1):
        for i in self.windowBlocks:
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.windowOffsetX-self.windowWidth/2.0,
                         self.startY+self.totalHeight/2.0+self.windowOffsetY+self.windowHeight/2.0))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.windowOffsetX-self.windowWidth/2.0,
                         self.startY+self.totalHeight/2.0+self.windowOffsetY+self.windowHeight/2.0-self.windowHeight*self.windowFac))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.windowOffsetX,
                         self.startY+self.totalHeight/2.0+self.windowOffsetY-self.windowHeight/2.0))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.windowOffsetX+self.windowWidth/2.0,
                         self.startY+self.totalHeight/2.0+self.windowOffsetY+self.windowHeight/2.0-self.windowHeight*self.windowFac))

            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.windowOffsetX+self.windowWidth/2.0,
                         self.startY+self.totalHeight/2.0+self.windowOffsetY+self.windowHeight/2.0))
            path.append('Z')

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)




    def CutLines(self, strokeColor='yellow', strokeWidth=1, clickout = 0.015*cm()):
        for i in self.cutLineBlocks:
            # ToDo: the clickout (2 paths)
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0, self.startY))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0, self.startY+self.totalHeight/2.0-clickout))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)

            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0, self.startY+self.totalHeight/2.0+clickout))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0, self.startY+self.totalHeight))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)


    def PlatFormSlits(self, strokeColor='red', strokeWidth=1):

        for i in self.platformSlitBlocks:
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0-self.platformSlitWidth/2.0,
                         self.startY))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0-self.platformSlitWidth/2.0,
                         self.startY+self.platformSlitHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.platformSlitWidth/2.0,
                         self.startY+self.platformSlitHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.blockWidth/2.0+self.platformSlitWidth/2.0,
                         self.startY))

            pathObj = svg.path.Path(d=path,stroke=strokeColor, stroke_width=strokeWidth, fill='none')
            self.drawing.add(pathObj)

    def BottomConnections(self, strokeColor='blue', strokeWidth=1):

        for i in self.connBlocksBot:
            path = []
            path.append(('M', self.startX+(i-1)*self.blockWidth+self.connWidth1, self.startY+self.totalHeight))
            path.append(('L', self.startX+(i-1)*self.blockWidth+self.connWidth2, self.startY+self.totalHeight-self.connHeight))

            path.append(('L', self.startX+i*self.blockWidth-self.connWidth2, self.startY+self.totalHeight-self.connHeight))
            path.append(('L', self.startX+i*self.blockWidth-self.connWidth1, self.startY+self.totalHeight))

            pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
            self.drawing.add(pathObj)


    def TopConnections(self, strokeColor='blue', strokeWidth=1):

        for i in range(1,self.blockHorzN+1):

            if i in self.noConnBlocksTop:
                pass

            elif i in self.connBlocksTop:
                path = []
                path.append(('M', self.startX+(i-1)*self.blockWidth, self.startY+self.connHeight))
                path.append(('L', self.startX+(i-1)*self.blockWidth+self.connWidth1, self.startY+self.connHeight))
                path.append(('L', self.startX+(i-1)*self.blockWidth+self.connWidth2, self.startY))

                pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
                self.drawing.add(pathObj)

                path = []
                path.append(('M', self.startX+i*self.blockWidth-self.connWidth2, self.startY))
                path.append(('L', self.startX+i*self.blockWidth-self.connWidth1, self.startY+self.connHeight))
                path.append(('L', self.startX+i*self.blockWidth, self.startY+self.connHeight))

                pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
                self.drawing.add(pathObj)

            else:
                path = []
                path.append(('M', self.startX+(i-1)*self.blockWidth, self.startY+self.connHeight))
                path.append(('L', self.startX+i*self.blockWidth, self.startY+self.connHeight))

                pathObj = svg.path.Path(d=path,stroke=strokeColor,stroke_width=strokeWidth,fill='none')
                self.drawing.add(pathObj)




if  __name__ =='__main__':
    print "do something"